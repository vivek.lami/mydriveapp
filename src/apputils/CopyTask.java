/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 
/**
 *
 * @author vivek
 */
package apputils;

import java.io.File;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import javafx.concurrent.Task;
import servercall.DownloadAllDataServiceCall;

/**
 *
 * @author vivek
 */
public class CopyTask extends Task<List<File>>{
    
    private String filepath;
    private DownloadAllDataServiceCall downloadAllDataService;
            
    public CopyTask(String path)
    {
        this.filepath = path;
        downloadAllDataService = new DownloadAllDataServiceCall("d:\\test");
    }
            
            
    @Override
    protected List<File> call() throws Exception {
      
        //System.out.println("Path"+allpath);
        File dir = new File(filepath);
        File[] files = dir.listFiles();
        int count = files.length;
        System.out.println("Files count-->"+ count);
 
        List<File> copied = new ArrayList<File>();
        int i = 0;
        for (File file : files) {
            if (file.isDirectory()|| file.isFile()) {
                this.copy(file);
                
                copied.add(file);
                System.out.println("Files count"+ copied.size());
            }
            i++;
            this.updateProgress(i, count);
        }
        return copied;
        //To change body of generated methods, choose Tools | Templates.
    }
    
    private void copy(File file) throws Exception {
        this.updateMessage("Downloading: " + file.getAbsolutePath());
        Thread.sleep(500);
    }
}
