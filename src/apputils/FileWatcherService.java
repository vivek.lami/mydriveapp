/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package apputils;

import com.sun.nio.file.SensitivityWatchEventModifier;
import controller.MainViewController;
import java.io.File;
import static java.nio.file.StandardWatchEventKinds.*;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import servercall.SyncingDeleteFoldersFilesServicesCall;
import servercall.SyncingFoldersFilesServiceCall;

/**
 *
 * @author vivek
 */
public class FileWatcherService {

    private  WatchService watcher;
    private Map<WatchKey, Path> keys;
    public ArrayList<File> allPathList;
    public ArrayList<File> allDeletedPathList;
    int fileAdded = 0;
    private String OSName;
    private SyncingFoldersFilesServiceCall syncingFoldersServices;
    private Path createdDirectory;
    private SyncingDeleteFoldersFilesServicesCall syncingDeleteServices;
    private MainViewController mainviewController;
 
    /**
     * Creates a WatchService and registers the given directory
     */
   public FileWatcherService() {
       syncingFoldersServices = new SyncingFoldersFilesServiceCall();
       syncingDeleteServices = new SyncingDeleteFoldersFilesServicesCall();
       mainviewController = new MainViewController();
    }
 
   public void startFileWatcherServices(Path dir)throws IOException
   {
        OSName = System.getProperty("os.name");
        this.watcher = FileSystems.getDefault().newWatchService();
        this.keys = new HashMap<WatchKey, Path>();
        this.createdDirectory = dir;
        allPathList = new ArrayList<>();
        allDeletedPathList = new ArrayList<>();
        walkAndRegisterDirectories(dir);
        try {
            processEvents();
        } catch (SQLException ex) {
            Logger.getLogger(FileWatcherService.class.getName()).log(Level.SEVERE, null, ex);
        }
   }
    /**
     * Register the given directory with the WatchService; This function will be called by FileVisitor
     */
    private void registerDirectory(Path dir) throws IOException 
    {
       // folder.register(watcher, new WatchEvent.Kind[]{StandardWatchEventKinds.ENTRY_MODIFY}, SensitivityWatchEventModifier.HIGH);
        WatchKey key = dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
         //WatchKey key = dir.register(watcher, new WatchEvent.Kind[]{ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY},SensitivityWatchEventModifier.HIGH);
        keys.put(key, dir);
    }
 
    /**
     * Register the given directory, and all its sub-directories, with the WatchService.
     */
    private void walkAndRegisterDirectories(final Path start) throws IOException {
        // register directory and sub-directories
        Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                registerDirectory(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }
 
    public void stopWatcherServices()throws IOException
    {
        allPathList.clear();
        watcher.close();
    }
    /**
     * Process all events for keys queued to the watcher
     */
   private void processEvents() throws SQLException {
       Path child = Paths.get("");
        Path currentChild = Paths.get("");
        for (;;) {
 
            // wait for key to be signalled
            WatchKey key = null;
            try {
                key = watcher.take();
            } catch (InterruptedException x) {
                return ;
            }
 
            Path dir = keys.get(key);
            if (dir == null) {
                System.err.println("WatchKey not recognized!!");
                continue;
            }
 
            for (WatchEvent<?> event : key.pollEvents()) {
                @SuppressWarnings("rawtypes")
                WatchEvent.Kind kind = event.kind();
 
                // Context for directory entry event is the file name of entry
                @SuppressWarnings("unchecked")
                Path name = ((WatchEvent<Path>)event).context();
                 child = dir.resolve(name);
 
                // print out event
          
                  // if directory is created, and watching recursively, then register it and its sub-directories
                     /// if(!child.toString().contains(".DS_Store"))
                     /// {
                            if(kind == ENTRY_CREATE)
                             {
                                 try
                                       {
                                           System.out.format("%s: %s\n", event.kind().name(), child);
                                          if(!child.toString().contains(".DS_Store"))
                                            {  String newcreatedFolder = child.toString().toLowerCase();
                                               if(!newcreatedFolder.contains("new folder") && !newcreatedFolder.contains("untitled folder"))
                                                {
                                                    
                                                 allPathList.add(child.toFile());
                                                 System.out.println("Created dirc-->"+createdDirectory);
                                                try {
                                                   // if(mainviewController.isNetworkAvailable() == 0)
                                                   // {
                                                        syncingFoldersServices.syncingFolder(allPathList, createdDirectory);
                                                   // }
                                                   // else
                                                   // {
                                                       // mainviewController.insertCreatedPathIntoDB(child.toString());
                                                    //}
                                                } catch (Exception ex) {
                                                    Logger.getLogger(FileWatcherService.class.getName()).log(Level.SEVERE, null, ex);
                                                 }
                                                 allPathList.clear();
                                                }
                                              }
                                        if (Files.isDirectory(child)) {
                                              
                                              walkAndRegisterDirectories(child);
                                        }
                                       }catch(IOException e)
                                       {
                                           e.printStackTrace();
                                       }

                             }
                            else if(kind == ENTRY_DELETE)
                            {
                                System.out.println("in delte");
                                System.out.format("%s: %s\n", event.kind().name(), child);

                                if(!allPathList.isEmpty())
                                     {
                                    for(int i=0;i<allPathList.size();i++)
                                     {
                                                                      //System.out.println("Before DELETe"+allPathList.toString());
                                         if(child.toString().equals(allPathList.get(i).toString()))
                                         {
                                              System.out.println("Before DELETe--"+child.toString());
                                              allPathList.remove(i);
                                        }

                                     }
                                 }
                                 String newcreatedFolder = child.toString().toLowerCase();
                                 //allDeletedPathList.add(child.toFile());
                                   //syncingDeleteServices.deletingFilesFolders(allDeletedPathList, createdDirectory);
                                 if(!newcreatedFolder.contains("new folder") && !newcreatedFolder.contains("untitled folder"))
                                {
                                   allDeletedPathList.add(child.toFile());
                                    try {
                                        //if(mainviewController.isNetworkAvailable() == 0)
                                        //{
                                            syncingDeleteServices.deletingFilesFolders(allDeletedPathList, createdDirectory);
                                        //}
                                        //else
                                        //{
                                          //  mainviewController.insertDeletedPathIntoDB(child.toString());
                                        //}
                                    } catch (Exception ex) {
                                        Logger.getLogger(FileWatcherService.class.getName()).log(Level.SEVERE, null, ex);
                                    }
                                   
                                   allDeletedPathList.clear();
                                   
                                }
                                // syncingDeleteServices.deletingFilesFolders(allDeletedPathList, createdDirectory);

                             }
                            else if(kind == ENTRY_MODIFY)
                            {
                                System.out.format("%s: %s\n", event.kind().name(), child);
                            }
                             System.out.println(" Final LIST---"+allPathList.toString());
                             System.out.println(" Deleted LIST---"+allDeletedPathList.toString());
                      }
             // reset key and remove from set if directory no longer accessible
            boolean valid = key.reset();
            if (!valid) {
                keys.remove(key);
                System.out.println("NO KEY INVALID-->"+valid);
                try {
                    if(OSName.equalsIgnoreCase("Mac"))
                    {
                    currentChild =  allPathList.get(allPathList.size()-1).toPath();
                    System.out.println("Invalid key---"+child);
                    walkAndRegisterDirectories(currentChild);
                    }
                } catch (IOException ex) {
                    Logger.getLogger(FileWatcherService.class.getName()).log(Level.SEVERE, null, ex);
                }
                 // all directories are inaccessible
                if (keys.isEmpty()) {
                    break;
                }
            }
        }
        
    }
   
   
   
   
}
