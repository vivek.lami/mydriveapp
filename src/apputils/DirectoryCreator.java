/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package apputils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 * @author vivek
 */
public class DirectoryCreator {

    public DirectoryCreator() {
    }
  
   public Path createDirectory(String userpath)
   {
       //C:\Users\vivek\
        Path path = Paths.get(userpath);
        // Mutiple path creation
        
        // Creation for Logic for SubDirectories
        //Path path = Paths.get("C:\\Directory2\\Sub2\\Sub-Sub2");
        //if directory exists?
         if (!Files.exists(path)) {
           try {
                Files.createDirectories(path);
                //Files.createFile(path);

                
           } catch (IOException e) {
                //fail to create directory
                e.printStackTrace();
           }
        }
        else
       {
            System.out.println("Path ---"+"Directory already exists");
            
       }
        return path;
   }
  
   public boolean  isType(String path)
   {
       boolean fileType = false;
      Path file = new File(path).toPath();
       //File file = new File(path);
       if(Files.exists(file))
       {
           if(Files.isDirectory(file))
           {
               fileType = true;
               System.out.println("Path ---"+" is a Folder ");
               return fileType;
           }
           else if(Files.isRegularFile(file))
           {
               fileType = false;
               System.out.println("$$$AASGFAS"+" is a File ");
               return fileType;
               
                        
           }
             
        }
       
      return fileType;

      
   }
}
