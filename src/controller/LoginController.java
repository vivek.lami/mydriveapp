/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author vivek
 */
import apputils.AESCrypt;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import constant.Constant;
import constant.DBConstant;
import database.BoonDriveDBConnection;
import java.lang.reflect.Type;
import java.security.GeneralSecurityException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import manager.LoginManager;
import javafx.event.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import model.LoginInfo;
import model.UserLogin;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.simple.JSONObject;
import servercall.ServerCallServices;


/** Controls the login screen */
public class LoginController {
    /**
     * These are the ID's that are defined and  used in FXML file "login.fxml"
     */
  @FXML private TextField user;
  @FXML private TextField password;
  @FXML private TextField subdomain;
  @FXML private Button loginButton;
  @FXML private Label lblValidatingError;
            
  private ServerCallServices servercall;
  private AESCrypt aesEncrypting;
   private BoonDriveDBConnection boonDriveDB;
   public LoginController() {
      servercall = new ServerCallServices("");
       aesEncrypting = new AESCrypt();
      boonDriveDB = BoonDriveDBConnection.getDatabaseConnectionInstances();
    }
  
  
  
  public void initialize() {}
  
  public void initManager(final LoginManager loginManager) {
    loginButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override public void handle(ActionEvent event) {
        String sessionID = authorize();
        if (sessionID != null) {
            
          loginManager.authenticated(sessionID);
            
         }
        else
        {
            lblValidatingError.setVisible(true);
        }
                
      }
    });
  }

  /**
   * Check authorization credentials.
   * 
   * If accepted, return a sessionID for the authorized session
   * otherwise, return null.
   */   
  private String authorize() {
      String serverRes ="";
      boolean loggedin = false;
      try {
          
              String userName = user.getText();
               String userPswrd  = password.getText();
               String userDomain = subdomain.getText();
               String encodedValue = aesEncrypting.encrypt(Constant.PASSWORD_ENCRYPTION_KEY, userPswrd);
               serverRes = servercall.loginServicesCall(getLoginJsonData(userName,encodedValue,userDomain));
               System.out.println("respons--"+serverRes);
               if(serverRes.contains("Failed to connect"))
                     {
                         lblValidatingError.setVisible(true);
                         lblValidatingError.setText("Problem in connection");
                     }
               else
               {
                  loggedin = parsedJSONData(serverRes);
               }
            } catch (Exception ex) {
                Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
         
             
         return   loggedin ? "" : null;
        ///return generateSessionID();
  }
     
  private static int sessionID = 0;

  private String generateSessionID() {
    sessionID++;
    
    return "xyzzy - session " + sessionID;
  }
  
  private boolean parsedJSONData(String response)
  {
      
      System.out.println("respons--"+response);
      boolean isSuccess = false;
      int loggedin = 1;
      try {
          Type collectionType = new TypeToken<UserLogin>() {
          }       .getType();
          org.json.JSONObject jsonObj = new org.json.JSONObject(response);
          GsonBuilder gb = new GsonBuilder();
          Gson gson = gb.create();
          UserLogin userlogin = gson.fromJson(jsonObj.toString(), collectionType);
          LoginInfo loginInfo =userlogin.getLoginInfoData();
          if(userlogin.isSuccess())
          {
              isSuccess = userlogin.isSuccess();
                      
             int professionalstatus = (loginInfo.getProfessional()) ? 1 : 0;
             int registerstatus = (loginInfo.getRegistered()) ? 1 : 0;
            String sqlQuery = "Insert into "+DBConstant.TABLE_NAME_USERLOGIN+"(apiKey,email,firstName,fullName,gender,lastName,professional,profilePic,registered,userId,userNumber,userSource,userType,isUserLoggedIn)"
                   +"Values('"+loginInfo.getApiKey()+"','"+loginInfo.getEmail()+"','"+loginInfo.getFirstName()+"','"+loginInfo.getFullName()+"','"
                    +loginInfo.getGender()+"','"+loginInfo.getLastName()+"',"+professionalstatus+ ",'"+loginInfo.getProfilePic()+"',"+registerstatus+
                    ",'"+loginInfo.getUserId()+"','"+loginInfo.getUserNumber()+"','"+loginInfo.getUserSource()+"','"+loginInfo.getUserType()+"',"+loggedin+")";
               
                  try {
                  boonDriveDB.create_insert(sqlQuery);
              } catch (SQLException ex) {
                  Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
              }
             return isSuccess;
          }
          else
          {
              lblValidatingError.setVisible(true);
              lblValidatingError.setText(userlogin.getMessage());
              
          }
         return isSuccess;
      } catch (JSONException ex) {
          Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
      }
      return isSuccess;
  }
  private JSONObject getLoginJsonData(String username,String userpswrd,String userdomain)
  {
     JSONObject loginJson = new JSONObject();
     
      try {
          loginJson.put(Constant.KEY_EMAIL, username);
          loginJson.put(Constant.KEY_PASSWORD, userpswrd);
          loginJson.put(Constant.KEY_SUBDOMAIN, userdomain);
          loginJson.put(Constant.KEY_DEVICESID, "");
      } catch (Exception ex) {
          Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
      }
     
     return loginJson;
      
  }
}
