/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author vivek
 */
import apputils.CopyTask;
import apputils.CryptoUtils;
import boondriveapp.BoonDriveApp;
import constant.DBConstant;
import database.BoonDriveDBConnection;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.util.List;
import javafx.concurrent.WorkerStateEvent;
import manager.LoginManager;
import javafx.event.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import servercall.DownloadAllDataServiceCall;
import javafx.concurrent.Task;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
//import net.jimmc.jshortcut.JShellLink;


/** Controls the main application screen */
public class MainViewController {
  @FXML private Button logoutButton;
  @FXML private Label  sessionLabel;
  @FXML public ProgressBar progressBar;
  @FXML
  private ProgressIndicator progressIndicator;
  @FXML
  private Label statusLabel;
  private CopyTask copyTask;
  private DownloadAllDataServiceCall downloadAllDataService;
  @FXML 
  private Button btncreatedirectory;
  @FXML
  private TextField directoryname;
  @FXML
  private Label copyLabel;
  private String filepath;
  private String OSName;
  private BoonDriveDBConnection boonDriveDB;
  private Connection dbConectionStatus;
  private PreparedStatement preparestatement;
  private ResultSet resultset;
  private String changepath;
  public String PRJT_PATH="";
  //private JShellLink link;
  private String filePath;
  public void initialize() {}

    public MainViewController() {
        boonDriveDB = BoonDriveDBConnection.getDatabaseConnectionInstances();
      try {
         int tablecreate = boonDriveDB.create_insert(DBConstant.CREATE_TABLE_USERLOGIN);
         System.out.println("Result status--"+tablecreate);
      } catch (SQLException ex) {
          Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
      }
         OSName = System.getProperty("os.name");
        
    }
    
  public void initSessionID(final LoginManager loginManager, String sessionID) {
   /// sessionLabel.setText(sessionID);
    
    logoutButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override public void handle(ActionEvent event) {
          try {
              loginManager.logout();
             } catch (Exception ex) {
              Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
          }
      }
    });
  }
  public void btnCreateDirectory(Stage stage)
  {
      
         //boonDriveDB.creatTable(dbConectionStatus);
    btncreatedirectory.setOnAction(new EventHandler<ActionEvent>() {
          @Override
          public void handle(ActionEvent event) {
              try {
                 try {
                 
                    System.out.println("User id0999"+constant.Constant.USER_ID);    
                 launchDirectoryChooser(stage); 
                filepath = directoryname.getText();
                changepath = setFilePathByOS();
                 downloadingWorkerThread(changepath);
                 btncreatedirectory.setVisible(false);
                 System.out.println("Created path:"+directoryname.getText());
                  savedDesktopPath(changepath);
                  //Platform.exit();
                      //To change body of generated methods, choose Tools | Templates.
                  } catch (Exception ex) {
                      Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
                  }
              } catch (Exception ex) {
                  Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
              }
          }
      });
  }
  private void launchDirectoryChooser(Stage scene)
  {
       DirectoryChooser directoryChooser = new DirectoryChooser();
                File selectedDirectory = 
                        directoryChooser.showDialog(scene);
                 
                if(selectedDirectory == null){
                    directoryname.setText("No Directory selected");
                }else{
                    directoryname.setText(selectedDirectory.getAbsolutePath());
                }
  }
  private void savedDesktopPath(String createdpath) throws SQLException
  {
      try
      {
         String rawQuery = "Update userLogin Set saved_path = '"+createdpath+"'";
         boonDriveDB.create_insert(rawQuery);
     }
      catch(Exception e)
      {
          System.out.println("SQL Exception--"+e);
      }
      finally{
                  
      }
  }
  public String getSavedDesktopPath()
  {
      String path = null;
      try
      {
         String rawQuery = "Select saved_path from "+DBConstant.TABLE_NAME_USERLOGIN;
         resultset = boonDriveDB.query(rawQuery);
        if(resultset.next())
         {
            path = resultset.getString("saved_path");
         }
         System.out.println("login path--"+path);
      }
      catch(Exception e)
      {
          System.out.println("SQL Exception--"+e);
      }
      finally{
          try {
              resultset.close();
          } catch (Exception ex) {
              Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
          }
      }
      return path;
  }
  
  public int isUserLoggedIn() throws SQLException
  {
       int status = 0;
      if(boonDriveDB!=null)
      {
      try
      {
          
          String sqlRawQuery = "Select isUserLoggedIn from "+DBConstant.TABLE_NAME_USERLOGIN;
          resultset = boonDriveDB.query(sqlRawQuery);
              
         if(resultset.next())
         {
            status = resultset.getInt("isUserLoggedIn");
         }
         System.out.println("login status--"+status);
      }
      catch(Exception e)
      {
          System.out.println("SQL Exception--"+e);
      }
      finally{
           resultset.close();
        }}
      return status;
  }
  
  public String getUserID()
  {
      String userID ="";
      String userName ="";
      try
      {
          
          String sqlRawQuery = "Select userId,fullName from "+DBConstant.TABLE_NAME_USERLOGIN;
          resultset = boonDriveDB.query(sqlRawQuery);
         if(resultset.next())
         {
            userID = resultset.getString("userId");
            userName = resultset.getString("fullName");
            constant.Constant.USER_NAME = userName;
            
         }
         System.out.println("User ID--"+userID);
         System.out.println("User Name--"+userName);
      }
      catch(Exception e)
      {
          System.out.println("SQL Exception--"+e);
      }
      finally{
          try {
               resultset.close();
              } catch (SQLException ex) {
              Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
          }
      }
      return userID;
  }
  
  public ArrayList<File> getAllCreatedData()
  {
      ArrayList<File> allList = new ArrayList<>();
      
      return allList;
  }
  
  public int isNetworkAvailable() throws InterruptedException
  {
      int isnetwork = 0;
      // register directory and process its events
        Process process;
      try {
           process = java.lang.Runtime.getRuntime().exec("ping www.google.com");
           isnetwork = process.waitFor();
           System.out.println("Connection status-->"+process.waitFor());
      } catch (IOException ex) {
          Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
      }
      return isnetwork;
        
  }
  public void showTheCreatedFolders(String changepath) throws IOException
  {
      System.out.println("fILE PATH-->"+changepath);
     File  file = new File (changepath);
      Desktop desktop = Desktop.getDesktop();
      desktop.open(file);
      System.out.println("OS NAme-->"+OSName);
//       if (OSName.toLowerCase().contains("win"))
//       {
//           
//           file = new File (changepath);
//           
//       }
//       else if(OSName.toLowerCase().contains("linux"))
//       {
//           file = new File (changepath);
//       }
//       else
//       {
//           file = new File (changepath);
//       }
       
 }
    //d:\tk\j
  ///"/home/manish/Desktop
   private String setFilePathByOS()
   {
         String osChangepath;
         int pos = filepath.lastIndexOf("");
             if (OSName.toLowerCase().contains("win"))
              {
                 
                  System.out.println("posi:"+pos);
                 osChangepath = filepath.substring(0, pos).concat("\\BoonDrive");
                System.out.println("Created path:"+osChangepath);
              }
             else if(OSName.toLowerCase().contains("linux"))
              {
                  //int pos = filepath.lastIndexOf("");
                  osChangepath =  filepath.substring(0, pos).concat("/BoonDrive");
              }
             else
             { // int pos = filepath.lastIndexOf("");
                 osChangepath =  filepath.substring(0, pos).concat("/BoonDrive");
             }
       return osChangepath;
   }
   
  public void showProgressBar()
  {   downloadingWorkerThread(changepath);
                   
  }
  private void downloadingWorkerThread(String path)
  {
      downloadAllDataService = new DownloadAllDataServiceCall(path);
          
      Task<Integer> task = new Task<Integer>() {
    @Override protected Integer call() throws Exception {
        int iterations =0;
        downloadAllDataService.serverGetCallServices();
         progressBar(path);
        for (iterations = 0; iterations < 1; iterations++) {
            
            if (isCancelled()) {
               break;
            }
            System.out.println("Iteration " + iterations);
        }
        
        return iterations;
    }
};
      new Thread(task).start();
      hideProgressItems();
      progressBar(path);
      
  }
  
  public void progressBar(String path)
  {
             showProgressItems();
             copyTask = new CopyTask(path);
             statusLabel.setMinWidth(250);
            statusLabel.setTextFill(Color.BLUE);
               // Unbind progress property
               progressBar.progressProperty().unbind();
 
               // Bind progress property
               progressBar.progressProperty().bind(copyTask.progressProperty());
               
               progressIndicator.progressProperty().unbind();
 
               // Bind progress property.
               progressIndicator.progressProperty().bind(copyTask.progressProperty());
 
               // Unbind text property for Label.
               statusLabel.textProperty().unbind();
 
               // Bind the text property of Label
                // with message property of Task
               statusLabel.textProperty().bind(copyTask.messageProperty());
 
               // When completed tasks
               copyTask.addEventHandler(WorkerStateEvent.WORKER_STATE_SUCCEEDED, //
             new EventHandler<WorkerStateEvent>() {
                 @Override
                 public void handle(WorkerStateEvent t) {
                     List<File> copied = copyTask.getValue();
                     statusLabel.textProperty().unbind();
                     statusLabel.setText("Copied: " + copied.size());
                     logoutButton.setVisible(false);
                     try
                     {
                      String storedpath =  getSavedDesktopPath();
                     showTheCreatedFolders(storedpath);
                     }catch(Exception e)
                     {
                         Logger.getLogger(BoonDriveApp.class.getName()).log(Level.SEVERE, null, e);
                     }
                 }
             });
                // Start the Task.
               new Thread(copyTask).start();
 
           }
    public void insertCreatedPathIntoDB(String createdpath) throws SQLException
    {
        String rawQuery = "Insert into Path (created_path) values ('"+createdpath+"')";
      try {
          resultset = boonDriveDB.query(rawQuery);
            
      } catch (SQLException ex) {
          Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
      }
      finally{
          resultset.close();
         
      }
        
    }
    public void insertDeletedPathIntoDB(String deletedpath) throws SQLException
    {
        String rawQuery = "Insert into Path (deleted_path) values ('"+deletedpath+"')";
      try {
        resultset = boonDriveDB.query(rawQuery);
          
      } catch (SQLException ex) {
          Logger.getLogger(MainViewController.class.getName()).log(Level.SEVERE, null, ex);
      }
      finally{
          resultset.close();
          
      }
        
    }
  
    public void showProgressItems()
    {
        progressIndicator.setVisible(true);
        progressBar.setVisible(true);
        copyLabel.setVisible(true);
        statusLabel.setVisible(true);
    }
    public void hideProgressItems()
    {
        progressIndicator.setVisible(false);
        progressBar.setVisible(false);
        copyLabel.setVisible(false);
        statusLabel.setVisible(false);
    }
    
    

  
}
