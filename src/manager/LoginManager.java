/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package manager;

/**
 *
 * @author vivek
 */

//import controller.MainViewController;
import controller.LoginController;
import controller.MainViewController;
import java.io.File;
import java.io.IOException;
import java.util.logging.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
//import net.jimmc.jshortcut.JShellLink;


/** Manages control flow for logins */
public class LoginManager {
  private Scene scene;
  private Stage stage;
  public String PRJT_PATH="";
 // private JShellLink link;
  private String filePath;
  @FXML private Button logoutButton;
  public LoginManager(Scene scene,Stage stage) {
    this.scene = scene;
    this.stage = stage;
     
  }

  /**
   * Callback method invoked to notify that a user has been authenticated.
   * Will show the main application screen.
   */ 
  public void authenticated(String sessionID) {
    showMainView(sessionID);
    
  }
@FXML
public void exitApplication(ActionEvent event) {
   ///Platform.exit();
   System.out.println("App closed event");
   
   
}
public void showDesktopIcon()
{
    System.out.println("Logout button visible"+logoutButton.isVisible());
   if(logoutButton.isVisible())
   {
   try {
          // showLoginScreen();
          getdir();
          //createDesktopShortcut();
      } catch (IOException ex) {
          Logger.getLogger(LoginManager.class.getName()).log(Level.SEVERE, null, ex);
      }
   }
}

//@FXML
//public void stop(){
//    System.out.println("Stage is closing");
//    scene.setOnMouseClicked(new EventHandler<MouseEvent>);
//    // Save file
//}
  /**
   * Callback method invoked to notify that a user has logged out of the main application.
   * Will show the login application screen.
   */ 
  public void logout() {
       // showLoginScreen();
//      try {
//         
//          System.out.println("Logout button visible"+logoutButton.isVisible());
//          getdir();
//          createDesktopShortcut();
//      } catch (IOException ex) {
//          Logger.getLogger(LoginManager.class.getName()).log(Level.SEVERE, null, ex);
//      }
   
  }
  
  public void showLoginScreen() {
    try {
      FXMLLoader loader = new FXMLLoader(LoginManager.class.getResource("/resources/login.fxml")
      );
      
//      Pane root = new Pane();
//        ImageView background = new ImageView(new Image("map.png", 1000, 800, false, true));
//        root.getChildren().add(background);
//        
//      StackPane root = new StackPane();
//      root.setStyle(
//            "-fx-background-image: url(" +
//                "'http://icons.iconarchive.com/icons/iconka/meow/256/cat-box-icon.png'" +
//            "); " +
//            "-fx-background-size: cover;"
//        );
      
      //scene.setRoot(root);
      scene.setRoot((Parent) loader.load());
      LoginController controller = 
        loader.<LoginController>getController();
     
     controller.initManager(this);
      
    } catch (IOException ex) {
      Logger.getLogger(LoginManager.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  

  private void showMainView(String sessionID) {
    try {
      FXMLLoader loader = new FXMLLoader(
        getClass().getResource("/resources/mainview.fxml")
      );
      
      scene.setRoot((Parent) loader.load());
       MainViewController controller = 
        loader.<MainViewController>getController();
      constant.Constant.USER_ID = controller.getUserID();
      //controller.initSessionID(this);
      controller.btnCreateDirectory(stage);
                       
    } catch (IOException ex) {
      Logger.getLogger(LoginManager.class.getName()).log(Level.SEVERE, null, ex);
    }
  
  }
  
  public void getdir() throws IOException{
    File f=new File(".");
    File[] f1=f.listFiles();
    PRJT_PATH=f.getCanonicalPath();
    System.out.println("FILE path"+PRJT_PATH+"");
}    //you can call this function at windowOpened event,this will get path of current directory where your project located.



//public void createDesktopShortcut() { 
////after that call createDesktopShortcut() function to create shortcut to desktop.
//    try {
//        link = new JShellLink();
//        filePath = JShellLink.getDirectory("") + PRJT_PATH +"\\dist\\BoonDrive.jar";
//    } catch (Exception e) {
//    }
//
//    try {
//        link.setFolder(JShellLink.getDirectory("desktop"));
//        link.setName("BoonDrive");  //Choose a name for your shortcut.In my case its JMM.
//        link.setPath(filePath); //link for our executable jar file
//        System.out.println("FILE path"+PRJT_PATH+"\\dist\\boondrive.ico");
//        link.setIconLocation(PRJT_PATH+"\\dist\\boondrive.ico"); //set icon image(before that choose your on manual icon file inside our project folder.[jmm.ico in my case])
//        link.save();
//      } catch (Exception ex) {
//       ex.printStackTrace();
//    }
//}
//  public void seperateWorkerThread()
//    {
//        
//        ExecutorService executorService = Executors.newFixedThreadPool(5);
//         executorService.execute(() -> {
//             System.out.println("Asynchronous task");
//            try {
//                
//                downloadAllDataService.serverGetCallServices();
//                  
//            }catch(Exception ex)
//            {
//                ex.printStackTrace();
//            }
//        });
//        executorService.shutdown();
//	// Wait until all threads are finish
//	while (!executorService.isTerminated()) {
//                    
// 	}
//        System.out.println("\nFinished all threads");
//        
   
  
  

   
}