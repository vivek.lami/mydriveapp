/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 *
 * @author vivek
 */
public class LoginInfo  implements Serializable{
    
    @SerializedName("success")
    private boolean success;
    @SerializedName("message")
    private String message;
    
    @SerializedName("api_key")
    private String apiKey;

   @SerializedName("_id")
    private String userId;

   @SerializedName("fullName")
   
    private String fullName;

    @SerializedName("firstName")
    
    private String firstName;

    @SerializedName("lastName")
   
    private String lastName;

    @SerializedName("user_type")
   
    private String userType;

    @SerializedName("email")
    
    private String email;

    @SerializedName("user_no")
    
    private int userNumber;

    @SerializedName("professional")
    
    private boolean professional;

    @SerializedName("profile_pic")
    
    private String profilePic;

    @SerializedName("gender")
    
    private String gender;

    @SerializedName("registered")
    
    private boolean registered;

    @SerializedName("user_source")
    
    private String userSource;
   
    
    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
    
    

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    
    
    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(int userNumber) {
        this.userNumber = userNumber;
    }

    public boolean getProfessional() {
        return professional;
    }

    public void setProfessional(boolean professional) {
        this.professional = professional;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public boolean getRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    public String getUserSource() {
        return userSource;
    }

    public void setUserSource(String userSource) {
        this.userSource = userSource;
    }
   
    
}
