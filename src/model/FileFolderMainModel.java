/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author vivek
 */
public class FileFolderMainModel  implements Serializable{
 
    @SerializedName("success")
    private boolean success;
    @SerializedName("message")
    private String message;
   
    @SerializedName("data")
    private List<FileFolder> driveModelList;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<FileFolder> getDriveModelList() {
        return driveModelList;
    }

    public void setDriveModelList(List<FileFolder> driveModelList) {
        this.driveModelList = driveModelList;
    }
}
