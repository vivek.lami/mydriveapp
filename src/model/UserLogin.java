/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 *
 * @author vivek
 */
public class UserLogin implements Serializable{
 
    @SerializedName("message")
    private String message;
    @SerializedName("success")
    private boolean success;

    @SerializedName("data")
    private LoginInfo loginInfoData;

    public LoginInfo getLoginInfoData() {
        return loginInfoData;
    }

    public void setLoginInfoData(LoginInfo loginInfoData) {
        this.loginInfoData = loginInfoData;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
    
    
}
