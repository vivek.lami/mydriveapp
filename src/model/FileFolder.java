/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable;

/**
 *
 * @author vivek
 */
public class FileFolder implements Serializable{
    
    
    private String file_name;

   
    private String fileTitle;

    
    private String file_id;

    
    private long file_size;

   @SerializedName("file_created_date")
    private String fileCreatedDate;

   @SerializedName("file_like")
    private int fileLike;

    @SerializedName("file_comments")
    private int fileComments;

    @SerializedName("uploaded_by_user")
    private boolean uploadedByUser;

    
    private String file_type;

   @SerializedName("file_path")
    private String filePath;

    @SerializedName("file_lar_path")
    private String fileLarPath;

   @SerializedName("file_org_path")
    private String fileOrgPath;
    @SerializedName("path")
    private String path;

    private String message;

    public FileFolder( ){
      
   }
    public String getFileName() {
        return file_name;
    }

    public void setFileName(String fileName) {
        this.file_name = fileName;
    }

    public String getFileTitle() {
        return fileTitle;
    }

    public void setFileTitle(String fileTitle) {
        this.fileTitle = fileTitle;
    }

    public String getFileId() {
        return file_id;
    }

    public void setFileId(String file_id) {
        this.file_id = file_id;
    }

    public long getFileSize() {
        return file_size;
    }

    public void setFileSize(long file_size) {
        this.file_size = file_size;
    }

    public String getFileCreatedDate() {
        return fileCreatedDate;
    }

    public void setFileCreatedDate(String fileCreatedDate) {
        this.fileCreatedDate = fileCreatedDate;
    }

    public int getFileLike() {
        return fileLike;
    }

    public void setFileLike(int fileLike) {
        this.fileLike = fileLike;
    }

    public int getFileComments() {
        return fileComments;
    }

    public void setFileComments(int fileComments) {
        this.fileComments = fileComments;
    }

    public boolean isUploadedByUser() {
        return uploadedByUser;
    }

    public void setUploadedByUser(boolean uploadedByUser) {
        this.uploadedByUser = uploadedByUser;
    }

    public String getFileType() {
        return file_type;
    }

    public void setFileType(String file_type) {
        this.file_type = file_type;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileLarPath() {
        return fileLarPath;
    }

    public void setFileLarPath(String fileLarPath) {
        this.fileLarPath = fileLarPath;
    }

    public String getFileOrgPath() {
        return fileOrgPath;
    }

    public void setFileOrgPath(String fileOrgPath) {
        this.fileOrgPath = fileOrgPath;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    
}
