/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author vivek
 */
/**
 * This class is a DBconnection class which uses Singleton designed pattern for connecting to the database
 */
import apputils.CryptoException;
import apputils.CryptoUtils;
import constant.DBConstant;
import java.io.File;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.sqlite.SQLiteConnection;
public class BoonDriveDBConnection {
    
  public Connection conn;
  private Statement statement;  
 private static BoonDriveDBConnection dbInstance = null;
 File encryptedFile;
 File inputFile;
 
 public static synchronized BoonDriveDBConnection getDatabaseConnectionInstances()
 {
     if(dbInstance == null)
     {
         dbInstance = new BoonDriveDBConnection();
     }
     return dbInstance;
 }
  private BoonDriveDBConnection()
  {
      try {
             Class.forName("org.sqlite.JDBC");
             this.conn = DriverManager.getConnection("jdbc:sqlite:"+DBConstant.DATABASE_NAME);
              inputFile = new File(DBConstant.DATABASE_NAME);
              encryptedFile = new File(DBConstant.DATABASE_NAME);
            } catch (Exception e) {
             System.out.println("DB connection error:"+e);
            }
  }
  public  Connection dbConnector() 
  {
      //E:\\BoonDriveDesktop\\JavaFXApplication1\\src\\resources\\BoondriveDesktop.sqlite
      //jdbc:sqlite:src\\resources\\BoondriveDesktop.sqlite
            try {
             Class.forName("org.sqlite.JDBC");
             Connection conn =DriverManager.getConnection("jdbc:sqlite:BoondriveDesktop.sqlite");
            
             return conn;
            } catch (Exception e) {
             System.out.println("DB connection error:"+e);
            }
//      try {
//      Class.forName("org.sqlite.JDBC");
//     Connection c = DriverManager.getConnection("jdbc:sqlite:test.db");
//    } catch ( Exception e ) {
//      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
//      System.exit(0);
//    }
//    System.out.println("Opened database successfully");
    return null;
  // TODO: handle exception
   }
//   public void creatTable(Connection dbconnection)
//   {
//       Statement stmt = null;
//    try {
//     
//      System.out.println("Opened database successfully");
//
//      stmt = dbconnection.createStatement();
////      String sql = "CREATE TABLE COMPANY " +
////                   "(ID INT PRIMARY KEY     NOT NULL," +
////                   " NAME           TEXT    NOT NULL, " + 
////                   " AGE            INT     NOT NULL, " + 
////                   " ADDRESS        CHAR(50), " + 
////                   " SALARY         REAL)"; 
//      String sql = "CREATE TABLE IF NOT EXISTS UserLogin(Id INTEGER PRIMARY KEY AUTOINCREMENT, apiKey TEXT, email TEXT, firstName TEXT, "
//              + "fullName TEXT, gender TEXT, lastName TEXT, professional INTEGER, "+ "profilePic TEXT, registered INTEGER, userId TEXT, userNumber INTEGER,"
//              + " userSource TEXT, userType TEXT,isUserLoggedIn INTEGER,saved_path TEXT)";
//      stmt.executeUpdate(sql);
//      
//    } catch ( Exception e ) {
//      System.err.println( e.getClass().getName() + ": " + e.getMessage() );
//      System.exit(0);
//    }
//    finally{
//           try {
//               stmt.close();
//               //dbconnection.close();
//           } catch (SQLException ex) {
//               Logger.getLogger(BoonDriveDBConnection.class.getName()).log(Level.SEVERE, null, ex);
//           }
//    }
//    System.out.println("Table created successfully");
//   }
   
   /**
     *
     * @param query String The query to be executed
     * @return a ResultSet object containing the results or null if not available
     * @throws SQLException
     */
    public ResultSet query(String query) throws SQLException{
        ResultSet res = null;
      try {
          decryptDBFile();
          statement = dbInstance.conn.createStatement();
           res = statement.executeQuery(query);
          encryptDBFile();
          return res;
      } catch (Exception ex) {
          Logger.getLogger(BoonDriveDBConnection.class.getName()).log(Level.SEVERE, null, ex);
      }
      return res;
    }
    /**
     * @desc Method to insert data to a table
     * @param insertQuery String The Insert query
     * @return boolean
     * @throws SQLException
     */
    public int create_insert(String insertQuery) throws SQLException {
         decryptDBFile();
        statement = dbInstance.conn.createStatement();
        int result = statement.executeUpdate(insertQuery);
          encryptDBFile();     
        return result;
 
    }
    
    public int create_insert(Object dataobj,String tablename) throws SQLException {
         decryptDBFile();
         int result =0;
        statement = dbInstance.conn.createStatement();
        ResultSet rs = statement.executeQuery("SELECT * FROM "+tablename);
        ResultSetMetaData rsmd = rs.getMetaData();
         int numberOfColumns = rsmd.getColumnCount();
         System.out.println( "Column count"+numberOfColumns );
         for(int i = 0;i<numberOfColumns ;i++)
         {
              //statement.set
            
          result = statement.executeUpdate("");
         }
          encryptDBFile();     
        return result;
 
    }
    public void encryptDBFile()
    {
       try {
            CryptoUtils.encrypt("Mary has one cat", inputFile, encryptedFile);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
    }
    public void decryptDBFile()
    {
      try {
          File decryptedFile = new File(DBConstant.DATABASE_NAME);
          CryptoUtils.decrypt("Mary has one cat", encryptedFile, decryptedFile);
      } catch (CryptoException ex) {
          Logger.getLogger(BoonDriveDBConnection.class.getName()).log(Level.SEVERE, null, ex);
      }
           
    }
    
}
