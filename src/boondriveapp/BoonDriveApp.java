/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package boondriveapp;

import apputils.FileWatcherService;
import apputils.MimeTypes;
import controller.MainViewController;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import manager.LoginManager;

/**
 *
 * @author vivek
 */
public class BoonDriveApp extends Application {
    private String OSName;
    private MimeTypes mimeTypes;
    private MainViewController mainviewController;
    private String savedpath;
     private FileWatcherService fileWatcherService;
    private ExecutorService execService;
    
    Pane pane;
    @Override
    public void start(Stage primaryStage) {
//############------------------------------------------------------------

   mainviewController = new MainViewController();
   OSName = System.getProperty("os.name");
   mimeTypes = new MimeTypes();

     StackPane pane = new StackPane();
     Scene scene = new Scene(pane);
    
     LoginManager loginManager = new LoginManager(scene,primaryStage);
     
     try {
            if(mainviewController.isUserLoggedIn()!=1)
            {
                loginManager.showLoginScreen();
                
                
//                BackgroundImage myBI= new BackgroundImage(new Image("my url",32,32,false,true),
//        BackgroundRepeat.REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT,
//          BackgroundSize.DEFAULT);
////then you set to your node
//               pane.setBackground(new Background(bimg));
               
                       
                primaryStage.getIcons().add(new Image("/resources/boondrive.png"));
                primaryStage.setTitle("BoonDrive");
                primaryStage.setResizable(false);
                primaryStage.setScene(scene);
                primaryStage.show();
                
            } 
            else
            {
                try {
                    
                    constant.Constant.USER_ID = mainviewController.getUserID();
                    System.out.println("User id--->"+constant.Constant.USER_ID);
                    savedpath = mainviewController.getSavedDesktopPath();
                    System.out.println("path--->"+savedpath);
                    mainviewController.showTheCreatedFolders(savedpath);
                    //
                    callWatcherService(savedpath);
                } catch (Exception ex) {
                    Logger.getLogger(BoonDriveApp.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(BoonDriveApp.class.getName()).log(Level.SEVERE, null, ex);
        }
     
     
     
     primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent event) {
                try
                {
                System.out.println("Stage is closing");
                savedpath = mainviewController.getSavedDesktopPath();
                mainviewController.showTheCreatedFolders(savedpath);
                
                 } catch (Exception ex) {
                    Logger.getLogger(BoonDriveApp.class.getName()).log(Level.SEVERE, null, ex);
                }
 
            }
        });
     
   
    }
    
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    /**
     * @param args the command line arguments
     */
  
    private void callWatcherService(String createdPath)
    {
       
     System.out.println("Create Path-- " + createdPath);       
      System.out.println("Current relative path is: ");
     execService = Executors.newFixedThreadPool(1);
     execService.submit(new Runnable() {
        @Override
        public void run() {
            System.out.println("thread is running...");
            Path dir = Paths.get(createdPath);
            try
            {
               fileWatcherService = new FileWatcherService();
               fileWatcherService.startFileWatcherServices(dir);
              
            }catch(IOException e)
            {
                e.printStackTrace();
            }
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }
    });
    }
    
}
