/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicethread;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;
import okhttp3.Call;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.simple.JSONObject;
import static servercall.ServerCallServices.JSON;
/**
 *
 * @author vivek
 */
public class RecussivePullServices {

    private final String serverParams;
    public RecussivePullServices(String path) {
         this.serverParams =  path;   
    }
     public String getCall()throws IOException
    {
       OkHttpClient client   = new OkHttpClient();
          HttpUrl.Builder urlBuilder = HttpUrl.parse(constant.Constant.BASE_URL+constant.Constant.KEY_ALL_ASSET_FILE).newBuilder();
            urlBuilder.addQueryParameter("path", serverParams);
            urlBuilder.addQueryParameter("offset", "0");
            urlBuilder.addQueryParameter("limit", "1000");
            urlBuilder.addQueryParameter("user_id", constant.Constant.USER_ID);
            String url = urlBuilder.build().toString();
            Request request = new Request.Builder()
                            .header("language", "en")
                            .header("device-type", "desktop")
                            .header("api-key", constant.Constant.API_KEY)
                             .url(url)
                        .get()
                        .build();
                    Response response = client.newCall(request).execute();
                 return response.body().string();
    }
     
    public String postOKHTTP(String path,String filename)
    {
                  try {
                      JSONObject obj = new JSONObject();
                       obj.put("name", filename);
                       obj.put("path", path);
                       obj.put("user_id", constant.Constant.USER_ID);
                       obj.put("user_name", constant.Constant.USER_NAME);
                      OkHttpClient.Builder builder = new OkHttpClient.Builder();
             builder.connectTimeout(180, TimeUnit.SECONDS);
            OkHttpClient client = builder.build();
                  RequestBody body = RequestBody.create(JSON, obj.toJSONString());
                    Request request = new Request.Builder()
                            .header("language", "en")
                            .header("device-type", "desktop")
                            .header("api-key", constant.Constant.API_KEY)
                        .url(constant.Constant.BASE_URL+constant.Constant.KEY_CREATE_FOLDER)
                        .post(body)
                        .build();
                    Response response = client.newCall(request).execute();
                    
                    return response.body().string();
                   
                }catch(Exception e)
                {
                    e.printStackTrace();
                }
                 return "";
    }
   
    
    public String uploadImage(String path,String filepath,String mimetype)throws IOException
    {
        RequestBody fileBody;
       // OkHttpClient client = new OkHttpClient();
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
             builder.connectTimeout(180, TimeUnit.SECONDS);
            OkHttpClient client = builder.build();
                
        MediaType MEDIA_TYPE =  getMediaType(mimetype);
        File sourceFile = new File(filepath);
        //File sourceFile = new File("D:\\Test\\hhh\\vbv\\nhh\\Desert.jpg");
            fileBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM).addFormDataPart("path", path)
            //.addFormDataPart("file", sourceFile.getPath(), RequestBody.create(MEDIA_TYPE_VIDEO, sourceFile))
                   .addFormDataPart("file", sourceFile.getPath(), RequestBody.create(MEDIA_TYPE, sourceFile))
            .build();
             Request request = new Request.Builder()
                            .header("language", "en")
                            .header("device-type", "desktop")
                            .header("file-count", "1")
                            .header("user-name", constant.Constant.USER_NAME)
                            .header("user-id", constant.Constant.USER_ID)
                            .header("api-key", constant.Constant.API_KEY)
                        .url(constant.Constant.BASE_URL+constant.Constant.KEY_UPLOAD_FILES)
                        .post(fileBody)
                        .build();
                    Response response = client.newCall(request).execute();
                    
                    return response.body().string();
    }
    
    public void saveMediaFiles(String urlpath,String saveDir,String filename)
    {
          String path = urlpath;
          System.out.println("VIDEO PATH"+path);
        //http://uatbdmobile.com:3001//jiten//j_vid_1348961012/j_thu_1348961012/Dove for men.mp4
         //String url1 = "http://uatbdmobile.com:3001/jiten//j_vid_1348961012/j_org_1348961012/Dove for men.mp4";
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
             builder.connectTimeout(180, TimeUnit.SECONDS);
            OkHttpClient client = builder.build();
            ///String url1 = "http://uatbdmobile.com:3001/jiten//j_oth_1348961012/j_org_1348961012/Abiraya Holi (Remake) - The Black Hawk _ New Nepal Bhasa - Newari Song 2015.m4a";                                   
            Call call = client.newCall(new Request.Builder().url(path).get().build());
            try
            {
                Response response = call.execute();
               InputStream  inputStream = response.body().byteStream();
                String saveFilePath = saveDir + File.separator + filename;
                System.out.println("Save path-->"+saveFilePath);
                FileOutputStream outputStream = new FileOutputStream(saveFilePath);
 
                int bytesRead = -1;
                byte[] buffer = new byte[4096];
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }
                 outputStream.close();
                inputStream.close();
                outputStream.flush();
                outputStream.close();
                }catch(IOException e)
                {
                    e.printStackTrace();
                }
                System.out.println("File downloaded");
    }
    
    private MediaType getMediaType(String mimetype)
    {
        MediaType mediatype = null;
        switch (mimetype)
        {
            case "image":
                mediatype = MediaType.parse("image/.*"); 
                break;
            case "text":
                mediatype = MediaType.parse("text/.*"); 
                break;
                
            case "video":
                mediatype = MediaType.parse("video/.*");
                break;
            case "audio":
                mediatype = MediaType.parse("audio/.*"); 
                break;
        }
        
//        if(mimetype.equals("image"))
//            {
//               mediatype = MediaType.parse("image/.*"); 
//            }
//            else if(mimetype.equals("text"))
//            {
//                mediatype = MediaType.parse("text/.*"); 
//            }
//            else if(mimetype.equals("video"))
//            {
//                mediatype = MediaType.parse("video/.*"); 
//            }
//            else if(mimetype.equals("audio"))
//            {
//                mediatype = MediaType.parse("audio/.*"); 
//            }
        return mediatype;
        
    }
    
}


