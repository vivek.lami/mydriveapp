/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servercall;

import apputils.DirectoryCreator;
import apputils.MimeTypes;
import boondriveapp.BoonDriveApp;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author vivek
 */
public class SyncingFoldersFilesServiceCall {

    private DirectoryCreator dircreate;
    private Path child;
    private String OSName;
    private MimeTypes mimeTypes;
    public SyncingFoldersFilesServiceCall() {
        dircreate = new DirectoryCreator();
        OSName = System.getProperty("os.name");
        mimeTypes = new MimeTypes();
        
    }
    
    /**
     * This method is used for the Syncing Folders from All OS ,parameter passed are the List of all folder created from root
     * any sub-folders within sub-folders.
     * @param allcreateFilelist
     * @param createdPath 
     */
    public void syncingFolder(ArrayList<File> allcreateFilelist,Path createdPath)
    {
        
        for(int i = 0 ;i<allcreateFilelist.size(); i++)
        {
            File value = allcreateFilelist.get(i);
            System.out.println(value.toString());
            System.out.println("File type--"+dircreate.isType(value.toString()));
            File file = new File(value.toString());
            Path parent = Paths.get(file.getParent());
            child = parent.resolveSibling("");
            String stripsStr = file.getParent().replace(createdPath.toString(),"");
            if(dircreate.isType(value.toString()))
            {
        
                    System.out.println("Parent path-->"+file.getParent());// 
                    String fileDirectoryName = file.getName();
                    System.out.println("Folder ->"+fileDirectoryName);
                    if (OSName.toLowerCase().contains("win"))
                    {

                       folderSyncingWindowsOS(child,stripsStr,file,value.toString());
                    }
                    else
                    {
                       folderSyncingLinux_MACOS(child,stripsStr,file,value.toString());
                     }
            }
            else
            {
                syncingFiles(file,stripsStr,value.toString(),child);
                System.out.println("Image file change"+value.toString());            
            }
        }
    }
    /**
     * This method is used for Syncing the folders created from WINDOWS OS 
     * it handles all the sub-folders created within the sub-folders
     * @param child
     * @param stripsStr
     * @param file
     * @param value 
     */
    private void folderSyncingWindowsOS(Path child,String stripsStr,File file,String value)
    {
        String windowsFilePath;
                if(child.toString().endsWith("\\"))
                  {
                    System.out.println("Root folder"+child.toString());
                     System.out.println("Root folder-->"+file.getParent());
                     windowsFilePath = "";
                     ///callFileSycingAsyncTask(dircreate.isType(value),file.getName(),"","");
                   }
                    else
                    {
                            System.out.println("Sub folder-->"+file.getParent());
                                           // String stripsStr = f.getParent().replace(createdPath.toString(),"");
                            System.out.println("Strips file-->"+stripsStr);
                            String changepath = stripsStr.replace("\\", "/");
                                System.out.println(" SUb Folder name->"+file.getName());
                            windowsFilePath = changepath.replaceFirst("/", "");
                        System.out.println(" Change->"+changepath.replaceFirst("/", ""));
                        ///callFileSycingAsyncTask(dircreate.isType(value),file.getName(),changeFilePath,"");
              }
                callFileSycingAsyncTask(dircreate.isType(value),file.getName(),windowsFilePath,"");
    }
    /**
     * This method is used for SYNCING the Folders created from the LINUX or MAC OS
     * here the necessary changes are done folders created within the sub-folders
     * @param child
     * @param stripsStr
     * @param file
     * @param value 
     */
    private void folderSyncingLinux_MACOS(Path child,String stripsStr,File file,String value)
    {
        String linuxFilePath;
        if(stripsStr.isEmpty())
        {
             System.out.println("Root folder in LINUX-->"+child.toString());
             System.out.println("Root folder in LINUX-->"+file.getParent());
             linuxFilePath = "";
             
             //callFileSycingAsyncTask(dircreate.isType(value),file.getName(),"","");
        }
        else
        {
             linuxFilePath = stripsStr.replaceFirst("/", "");
            System.out.println("Sub folder--> in LINUX-->"+file.getParent());
            System.out.println("Strips file--> in LINUX-->"+linuxFilePath);
             //callFileSycingAsyncTask(dircreate.isType(value),file.getName(),stripsStr.replaceFirst("/", ""),"");
            
        }
        callFileSycingAsyncTask(dircreate.isType(value),file.getName(),linuxFilePath,"");
    }
    /**
     * This method is used to give the call for Server for SYNCING the folders created ,it also handles the any files created
     * 
     * @param folder
     * @param filename
     * @param path
     * @param mimetype 
     */
    private void callFileSycingAsyncTask(boolean folder,String filename,String path,String mimetype)
    {
        ExecutorService syncingexecutorService = Executors.newFixedThreadPool(5);
        ServerCallServices serv = new ServerCallServices(path);
        syncingexecutorService.submit(new Runnable() {
            @Override
            public void run() {
                if(folder)
                {
                         String resposne = serv.postServiceCall(path, filename,"dir");
                         System.out.println("\n SYNCING FOLDER-->"+resposne);
                }
                else
                {
                    try
                    {
                        System.out.println("\n SYNCING FILES ");
                        System.out.println("\n SYNCING FILES PATH--"+path);
                       String response = serv.uploadImage(path, filename,mimetype);
                        System.out.println("\n Server response-- "+response);
                    }catch(IOException e)
                    {
                        e.printStackTrace();
                                
                    }
                            
                }
                        
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        syncingexecutorService.shutdown();
		// Wait until all threads are finish
		while (!syncingexecutorService.isTerminated()) {
 
		}
		System.out.println("\nSYNCING Finished all threads");
       
    }
    
    private void syncingFiles(File file,String stripsStr,String value,Path child)
    {
       System.out.println(file.getParent());// 
       System.out.println(file.getName());
       try
       {
          System.out.println("Root Image child--"+child.toString());
          Path source = Paths.get(value.trim());
          System.out.println("FileName ->Source---"+source);
          String fileMIMETYPE =  checkMIMEType(source,value);
          System.out.println("FileName  ->MIME---"+fileMIMETYPE);
          if (OSName.toLowerCase().contains("win")) 
          {
              filesSyncingForWindowOS(child,value,file,stripsStr,fileMIMETYPE);      
          }
          else
          {
             filesSyncingForLinus_MACOS(value,stripsStr,fileMIMETYPE);   
          }
           //callFileSycingAsyncTask(dircreate.isType(value.toString()),value.toString(),changeFilePath,type);
          }catch(Exception e)
          {
             e.printStackTrace();
           }
                               
                             
    }
    private void filesSyncingForWindowOS(Path child,String value,File file,String stripsStr,String fileMIMETYPE)
    { String windowsFilePath;
        System.out.println("Value-->"+value);
        if(child.toString().endsWith("\\"))
        {
          ///callFileSycingAsyncTask(dircreate.isType(value.toString()),value.toString(),"",fileMIMETYPE);
          windowsFilePath = "";
          System.out.println(" root image");
        }
        else
        {
          System.out.println("Sub folder-->"+file.getParent());
                                                  //String stripsStr = f.getParent().replace(createdPath.toString(),"");
           System.out.println("Strips file-->"+stripsStr);
           String changepath = stripsStr.replace("\\", "/");
          windowsFilePath = changepath.replaceFirst("/", "");
          //callFileSycingAsyncTask(dircreate.isType(value.toString()),value.toString(),changeFilePath,fileMIMETYPE);
         System.out.println(" sub folder image-->"+windowsFilePath);
        }
        callFileSycingAsyncTask(dircreate.isType(value),value,windowsFilePath,fileMIMETYPE);
    }
    
    private void filesSyncingForLinus_MACOS(String value,String stripsStr,String fileMIMETYPE)
    {
        String linuxFilePath;
        System.out.println(" root image link in LINUX-->"+value.toString());
        if(stripsStr.isEmpty())
        {
            linuxFilePath = "";
           //callFileSycingAsyncTask(dircreate.isType(value.toString()),value.toString(),"",fileMIMETYPE);
           System.out.println(" root image in LINUX");
        }
        else
        {
          System.out.println(" sub folder Strips file--> in LINUX-->"+stripsStr.replaceFirst("/", ""));
                                           //System.out.println(" sub folder image--> in LINUX"+stripsStr);
          linuxFilePath = stripsStr.replaceFirst("/", "");
          
        }
         callFileSycingAsyncTask(dircreate.isType(value),value,stripsStr.replaceFirst("/", ""),fileMIMETYPE);
    }
    private String checkMIMEType(Path source,String value)
    {
        String filemimetype = "";
        try {
            filemimetype = Files.probeContentType(source);
            if(filemimetype == null)
          {
                                    
                String  fileext = getFileExtention(value);
                 filemimetype = mimeTypes.lookupMimeType(fileext);
                System.out.println("FileEXten ->MIME TYPE---"+fileext);
               System.out.println("FileName if null ->MIME---"+filemimetype);
           }
        } catch (IOException ex) {
            Logger.getLogger(BoonDriveApp.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return filemimetype;
    }
    
     private String getFileExtention(String fileName)
    {
        System.out.println("\nSYNCING File Name"+fileName);
        if(fileName.contains("."))
        {
        String tets = fileName.substring(fileName.lastIndexOf('.'), fileName.length());
        System.out.println("\nSYNCING Extension -->"+tets);
        return fileName.substring(fileName.lastIndexOf('.'), fileName.length()).replaceFirst(".", "");
        }
        else
        {
            return "";
        }
              
    }
    
}
