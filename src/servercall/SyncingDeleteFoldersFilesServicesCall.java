/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servercall;

import apputils.DirectoryCreator;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *
 * @author vivek
 */
public class SyncingDeleteFoldersFilesServicesCall {

    private DirectoryCreator dircreate;
    private Path child;
    private String OSName;
    public SyncingDeleteFoldersFilesServicesCall() {
        dircreate = new DirectoryCreator();
        OSName = System.getProperty("os.name");
    }
    
    /**
     * This method is used for Deleting the files and folder for the Desktop and giving the call to Server 
     * @param deletedlist 
     * @param createdPath 
     */
    public void deletingFilesFolders(ArrayList<File> deletedlist,Path createdPath)
    {
        boolean fileType;
        for(int i = 0 ;i<deletedlist.size(); i++)
        {
           File value = deletedlist.get(i);
           System.out.println(value.toString());
           fileType = checkDeletedFileType(value.toString());
           System.out.println("File Delete type--"+fileType);
           File f = new File(value.toString());
           Path parent = Paths.get(f.getParent());
           child = parent.resolveSibling("");
           String stripsStr = f.getParent().replace(createdPath.toString(),"");
           if(fileType)
           {
             System.out.println("Parent path-->"+f.getParent());// 
             String fileDirectoryName = f.getName();
             System.out.println("Delete fileName ->"+fileDirectoryName);
             syncingDeletedData(child,f,stripsStr,fileType);
            }
            else
            {
              syncingDeletedData(child,f,stripsStr,fileType);

            }
        }
        
    }
    private void syncingDeletedData(Path child,File file,String stripsStr,boolean fileType)
    {
        if (OSName.toLowerCase().contains("win")) {
          syncingDeleteWindows_OS(child,file,stripsStr,fileType);
        }
        else
        {
          syncingDeleteLinux_MAC_OS(file,stripsStr,fileType);
         }
    }
    private void syncingDeleteWindows_OS(Path child,File file ,String stripsStr,boolean fileType)
    {
        String windowsFilePath;
        if(child.toString().endsWith("\\")  )
        {
            System.out.println("Root delete folder"+child.toString());
            System.out.println("Root delete folder-->"+file.getParent());
            windowsFilePath = "";
            
        }
        else
        {
           System.out.println("Sub delete folder-->"+file.getParent());
           System.out.println("Strips file-->"+stripsStr);
           String changepath = stripsStr.replace("\\", "/");
           System.out.println(" SUb delete Folder name->"+file);
           System.out.println(" Change->"+changepath.replaceFirst("/", ""));
           windowsFilePath =  changepath.replaceFirst("/", "");;
           
                                          
        }
        deleteFilesFolder(windowsFilePath,file.getName(),fileType);
    }
    private void syncingDeleteLinux_MAC_OS(File file ,String stripsStr,boolean fileType)
    {
        String linuxFilePath;
        /**
        * This condition is used for giving the call to Server for LINUX or MAC OS
        */
         if(stripsStr.isEmpty())
         {
           linuxFilePath = "";
          System.out.println(" root image in LINUX");
          }
        else
         {                                               
            System.out.println(" sub folder Strips file--> in LINUX-->"+stripsStr.replaceFirst("/", ""));
            //System.out.println(" sub folder image--> in LINUX"+stripsStr);
            linuxFilePath = stripsStr.replaceFirst("/", "");
        }
          deleteFilesFolder(linuxFilePath,file.getName(),fileType);
    }
    private void deleteFilesFolder(String path,String filename,boolean filetype)
    {
        ExecutorService syncingexecutorService = Executors.newFixedThreadPool(5);
        ServerCallServices serv = new ServerCallServices(path);
        syncingexecutorService.submit(new Runnable() {
            @Override
            public void run() {
                
                String response = serv.deleteFileFolder(path, filename,filetype);
                 System.out.println("\n Deleting FOLDER-->"+response);
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        });
        syncingexecutorService.shutdown();
        while (!syncingexecutorService.isTerminated()) {
                    
 		}
		System.out.println("\nFinished all Deleting threads");
   }
    /**
     * This method is used to check the file type and comparison is done
     * for differentiating whether it is a FILE or FOLDER.
     * @param filepath
     * @return 
     */
    private boolean checkDeletedFileType(String filepath)
    {
        boolean isAFile;
        String  fileext = getFileExtention(filepath);
          if(fileext.isEmpty())
          {
             isAFile = true; 
             return isAFile;
          }
          else
          {
             isAFile = false;
             return isAFile;
              
          }
             
    }
    /**
     * This method is used for getting the file extension and check whether it is 
     * a folder or file and on that basis the Syncing is done
     * @param fileName
     * @return 
     */
    private String getFileExtention(String fileName)
    {
        System.out.println("\nSYNCING deleted File Name-->"+fileName);
        if(fileName.contains("."))
        {
            String tets = fileName.substring(fileName.lastIndexOf('.'), fileName.length());
            System.out.println("\nSYNCING deleted File Extension-->"+tets);
            return fileName.substring(fileName.lastIndexOf('.'), fileName.length()).replaceFirst(".", "");
        }
        else
        {
            return "";
        }
          
    }
        
    
}
