/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servercall;

import apputils.DirectoryCreator;
import apputils.FileWatcherService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import controller.MainViewController;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import model.FileFolder;
import model.FileFolderMainModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import servicethread.RecussivePullServices;

/**
 *
 * @author vivek
 */
public class DownloadAllDataServiceCall {

    private DirectoryCreator dircreate;
    private Path child;
    private String OSName;
    private String createdPath;
    static FileWatcherService fileWatcherService;
    static ExecutorService execService;
     private MainViewController mainViewController ;
    public DownloadAllDataServiceCall(String path) {
        dircreate = new DirectoryCreator();
        OSName = System.getProperty("os.name");
        this.createdPath = path;
        mainViewController = new MainViewController();
    }
        
    private void callFileFolderASyncTask(String filepath)
    {
        
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        RecussivePullServices recur = new RecussivePullServices(filepath);
        //ServerCallServices recur = new ServerCallServices(); 
         executorService.execute(() -> {
             System.out.println("Asynchronous task");
            try {
                Type collectionType = new TypeToken<FileFolderMainModel>() {
                }       .getType();
                String resposne = recur.getCall();
                JSONObject jsonObj = new JSONObject(resposne);
                GsonBuilder gb = new GsonBuilder();
                Gson gson = gb.create();
                FileFolderMainModel obj =  gson.fromJson(jsonObj.toString(), collectionType);
                List<FileFolder> fileFolderobj = obj.getDriveModelList();
                System.out.println("REcursive ServerResponse"+resposne);
                if (!fileFolderobj.isEmpty()) {
                    for (int i = 0; i < fileFolderobj.size(); i++) {
                        FileFolder  folderobj = fileFolderobj.get(i);
                        String filepath1 = folderobj.getPath()+"/"+folderobj.getFileName();
                        callRecursiveServices(folderobj, filepath1, createdPath, recur);
                    }
                }
            }catch(JSONException e)
            {
                e.printStackTrace();
            }catch(IOException ex)
            {
                ex.printStackTrace();
            }
        });
        executorService.shutdown();
	// Wait until all threads are finish
	while (!executorService.isTerminated()) {
                    
 	}
        System.out.println("\nFinished all threads");
		
    }
    
    private void callRecursiveServices(FileFolder folderobj,String filepath,String createdPath,RecussivePullServices recur)
    {
        String changePathForOS;
        if(folderobj.getFileType().equals("dir"))
        {
                if (OSName.toLowerCase().contains("win")) {

                    System.out.println("Folder-- sub folder--"+folderobj.getFileName());
                    System.out.println("FILE path--"+filepath);
                                         ///String filepath = fileFolderobj.get(i).getPath()+"/"+fileFolderobj.get(i).getFileName();
                    dircreate.createDirectory(createdPath+"\\"+folderobj.getPath()+"\\"+folderobj.getFileName());
                    System.out.println("Create path Recursive-->"+createdPath);
                    System.out.println("Recursive call");
                }
                else
                {
                    dircreate.createDirectory(createdPath+"/"+filepath);
                    System.out.println("Recursive call OS Name"+OSName);
                }
               callFileFolderASyncTask(filepath);
        }
        else
        {
            String url = constant.Constant.BASE_URL+folderobj.getFileOrgPath();
               if (OSName.toLowerCase().contains("win")) {
                    System.out.println("File path--"+url);
                    changePathForOS = createdPath+"\\"+folderobj.getPath();
                    System.out.println("Whole path--"+changePathForOS);
                }
                else
                {
                    changePathForOS = createdPath+"/"+folderobj.getPath();
                    System.out.println("File path--"+url);
                    System.out.println("Whole path--"+changePathForOS);
                }
                recur.saveMediaFiles(url, changePathForOS,folderobj.getFileName());
                                   //recur.saveMedaiFiles(filepath, filepath, fileName);
        }
    }
    private String downloadFolders(FileFolder  folderobj,String createdPath)
    {      
        Path filepath;
        String changePath;
        System.out.println("Folder-- Main--"+folderobj.getFileName());
        if (OSName.toLowerCase().contains("win")) {
             changePath = createdPath+"\\"+folderobj.getFileName();
             dircreate.createDirectory(createdPath+"\\"+folderobj.getFileName());
             System.out.println("Create path -->"+createdPath);
        }
        else
        {
           changePath = createdPath+"/"+folderobj.getFileName();
           System.out.println("Create path -->"+createdPath);
        }
      filepath =  dircreate.createDirectory(changePath);
      System.out.println("File path##3"+filepath);
       callFileFolderASyncTask(folderobj.getFileName());
        /// mainViewController.progressBar(filepath.toString());
        return filepath.toString();
        
    }
    private String  downloadAllFiles(FileFolder  folderobj,ServerCallServices serv)
    {
        String filepath;
        String newPath;
        String url = constant.Constant.BASE_URL+folderobj.getFileOrgPath();
        if (OSName.toLowerCase().contains("win")) {
                 newPath = createdPath+"\\";
               System.out.println("ORG path--"+folderobj.getFileOrgPath());
                               
        }
        else
        {
            newPath = createdPath+"/";
            System.out.println("ORG path--"+folderobj.getFileOrgPath());
        }
        filepath = serv.saveMedaiFiles(url, newPath,folderobj.getFileName());
        return filepath;
    }
    
    private void callWatcherService(String createdPath)
    {
       
     System.out.println("Create Path-- " + createdPath);       
      System.out.println("Current relative path is: ");
     execService = Executors.newFixedThreadPool(1);
     execService.submit(() -> {
         System.out.println("thread is running...");
         Path dir = Paths.get(createdPath);
         try
         {
             fileWatcherService = new FileWatcherService();
             fileWatcherService.startFileWatcherServices(dir);
             
         }catch(IOException e)
         {
             e.printStackTrace();
         }
         throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
     });
    }
    public String serverGetCallServices()
    {
        String filepath = null;
        String result;
        try
                {   String path ="";
                    ServerCallServices serv = new ServerCallServices(path);
                    result = serv.getServicesCall();
                    System.out.println("ALL LIST--"+result);
                try
                {
                 Type collectionType = new TypeToken<FileFolderMainModel>() {
                      }       .getType();
                 
                   JSONObject jsonObj = new JSONObject(result);
                                               
                  JSONArray jsonObj1 = jsonObj.getJSONArray("data");
                  System.out.println("JSON Array"+jsonObj1.toString());
                   GsonBuilder gb = new GsonBuilder();
                   Gson gson = gb.create();                  
                   FileFolderMainModel obj =  gson.fromJson(jsonObj.toString(), collectionType);
                   System.out.println("ALL LIST--"+obj.getDriveModelList().toString());
                   List<FileFolder> fileFolderobj = obj.getDriveModelList();
                   for(int i =0;i < fileFolderobj.size();i++)
                   {
                       //dircreate = new DirectoryCreator();
                       System.out.println("Created Path-->"+createdPath);
                       FileFolder  folderobj = fileFolderobj.get(i);
                       if(folderobj.getFileType().equals("dir"))
                       {
                          filepath = downloadFolders(folderobj,createdPath);
                               
                        }
                       else
                       {
                         filepath =  downloadAllFiles(folderobj,serv);
                        }
                       
                   }
                   callWatcherService(createdPath);
                                 
                }catch(Exception e)
                {
                    e.printStackTrace();
                }
                 
                }catch(IOException e)
                {
                    e.printStackTrace();
                } 
        return filepath;
        
    }
    
}
