/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servercall;

import apputils.AESCrypt;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.TimeUnit;
import okhttp3.Call;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author vivek
 */
public class ServerCallServices {
    private AESCrypt aesEncrypting;

    public static final MediaType JSON   = MediaType.parse("application/json");
    private final String serverParams;
    public ServerCallServices(String path) {
        this.serverParams =  path; 
        aesEncrypting = new AESCrypt();
    }
    public String loginServicesCall(JSONObject loginjsonobj)
    {
        String error = "";
        try {
            /**
             * This snippet is used while Encrypting the Data Send to server
             */
            String encrytpt = aesEncrypting.encrypt(constant.Constant.PASSWORD_ENCRYPTION_KEY, loginjsonobj.toString());
            JSONObject obj = new JSONObject();
            obj.put("mydata", encrytpt);
            System.out.println("Encrypt---"+obj.toString());
            
             OkHttpClient.Builder builder = new OkHttpClient.Builder();
             builder.connectTimeout(180, TimeUnit.SECONDS);
            OkHttpClient client = builder.build();
                  RequestBody body = RequestBody.create(JSON, loginjsonobj.toJSONString());
                    Request request = new Request.Builder()
                            .header("language", "en")
                            .header("device-type", "desktop")
                            .url(constant.Constant.BASE_URL+constant.Constant.KEY_LOGIN)
                        .post(body)
                        .build();
                    Response response = client.newCall(request).execute();
                    
                    return response.body().string();
                 }catch(Exception e)
                {
                    e.getCause();
                    error = e.toString();
                    System.out.println("Server rrreee"+e);
                }
                 return error;
        
    }
     public String getServicesCall()throws IOException
    {
        try
        {
        OkHttpClient client = new OkHttpClient();
          HttpUrl.Builder urlBuilder = HttpUrl.parse(constant.Constant.BASE_URL+constant.Constant.KEY_ALL_ASSET_FILE).newBuilder();
            urlBuilder.addQueryParameter("path", serverParams);
            urlBuilder.addQueryParameter("offset", "0");
            urlBuilder.addQueryParameter("limit", "1000");
            urlBuilder.addQueryParameter("user_id", constant.Constant.USER_ID);
            String url = urlBuilder.build().toString();
            Request request = new Request.Builder()
                            .header("language", "en")
                            .header("device-type", "desktop")
                            .header("api-key", constant.Constant.API_KEY)
                             .url(url)
                        .get()
                        .build();
                    Response response = client.newCall(request).execute();
                    return response.body().string();
        }catch(Exception e)
        {
            e.printStackTrace();
        }
         return "";
                    
    }
     public String postServiceCall(String path,String filename,String filetype)
    {
                 
                      /**
         * This json object is ued for collecting the Folder created path and the created FileName, store in array of object
         */
                JSONArray jsonArray = new JSONArray();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("name",filename);
                    jsonObject.put("path",path);
                    jsonArray.put(jsonObject);

          
                      
                      JSONObject obj = new JSONObject();
                       //obj.put("name", filename);
                       //obj.put("path", path);
                       obj.put("user_id", constant.Constant.USER_ID);
                       obj.put("user_name", constant.Constant.USER_NAME);
                       obj.put("data",jsonArray);
                       System.out.println("JSON OBJECT ARRAy--"+obj);
                      OkHttpClient.Builder builder = new OkHttpClient.Builder();
             builder.connectTimeout(180, TimeUnit.SECONDS);
            OkHttpClient client = builder.build();
                  RequestBody body = RequestBody.create(JSON, obj.toJSONString());
                    Request request = new Request.Builder()
                            .header("language", "en")
                            .header("device-type", "desktop")
                            .header("api-key", constant.Constant.API_KEY)
                        .url(constant.Constant.BASE_URL+constant.Constant.KEY_CREATE_FOLDER)
                        .post(body)
                        .build();
                    Response response = client.newCall(request).execute();
                    
                    return response.body().string();
                   // String postResponse = doPostRequest(constant.Constant.BASE_URL+constant.Constant.KEY_CREATE_FOLDER,obj.toJSONString());
                         //  System.out.println("SERVER RESPOSNE"+postResponse);
                }catch(Exception e)
                {
                    e.printStackTrace();
                }
                 return "";
    }
   
    
    public String uploadImage(String path,String filepath,String mimetype)throws IOException
    {
        RequestBody fileBody;
       // OkHttpClient client = new OkHttpClient();
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
             builder.connectTimeout(300, TimeUnit.SECONDS);
            OkHttpClient client = builder.build();
        //C:\Users\\Public\Videos\Sample Videos\Wildlife.wmv
        //"C:\\Users\\vivek\\Downloads\\DisplayingBitmaps\\screenshots\\gridview.png"
        //"C:\\Users\\vivek\\BoonDrive\\Jellyfish.jpg"
        //C:\\Users\\vivek\\Downloads\\template.doc
        //C:\Users\vivek\Downloads\Crazy.mp3
        //C:\\Users\\vivek\\Desktop\\facebook_ringtone_pop.m4a
        //C:\Users\vivek\Desktop\facebook_ringtone_pop.m4a
        //"path": [{"path": ""},{"path": "dcc"}]
//        Mulitple folder uploading files 
//                .setType(MultipartBody.FORM).addFormDataPart("path", "[{\"path\": \"\"},{\"path\": \"a\"}]")
                
        File sourceFile = new File(filepath);
        //File sourceFile = new File("D:\\Test\\hhh\\vbv\\nhh\\Desert.jpg");
            fileBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM).addFormDataPart("path", path)
            //.addFormDataPart("file", sourceFile.getPath(), RequestBody.create(MEDIA_TYPE_VIDEO, sourceFile))
                   .addFormDataPart("file", sourceFile.getPath(), RequestBody.create(MediaType.parse(mimetype), sourceFile))
            .build();
             Request request = new Request.Builder()
                            .header("language", "en")
                            .header("device-type", "desktop")
                            .header("file-count", "1")
                            .header("user-name", constant.Constant.USER_NAME)
                            .header("user-id", constant.Constant.USER_ID)
                            .header("api-key", constant.Constant.API_KEY)
                        .url(constant.Constant.BASE_URL+constant.Constant.KEY_UPLOAD_FILES)
                        .post(fileBody)
                        .build();
                    Response response = client.newCall(request).execute();
                    
                    return response.body().string();
    }
    
    public String saveMedaiFiles(String urlpath,String filepath,String filename)
    {
          OkHttpClient client = new OkHttpClient();
          String saveFilePath = "";
            ///String url1 = "http://uatbdmobile.com:3001/jiten//j_oth_1348961012/j_org_1348961012/Abiraya Holi (Remake) - The Black Hawk _ New Nepal Bhasa - Newari Song 2015.m4a";                                   
            Call call = client.newCall(new Request.Builder().url(urlpath).get().build());
            try
            {
                Response response = call.execute();
                InputStream inputStream = null;
                inputStream = response.body().byteStream();
                 saveFilePath = filepath + File.separator + filename;
                System.out.println("Media file save path->"+saveFilePath);
                FileOutputStream outputStream = new FileOutputStream(saveFilePath);
 
                int bytesRead = -1;
                byte[] buffer = new byte[4096];
                while ((bytesRead = inputStream.read(buffer)) != -1) {
                    outputStream.write(buffer, 0, bytesRead);
                }
                outputStream.close();
                inputStream.close();
                outputStream.flush();
                inputStream.close();
                }catch(IOException e)
                {
                    e.printStackTrace();
                }
                System.out.println("File downloaded");
        return saveFilePath;
    }
    public String deleteFileFolder(String path,String filename,boolean filetype)
    {   String apiCall;
       try {
                        apiCall = checkTheFileType(filetype);
                        
                        /**
//         * This json object is ued for collecting the Folder created path and the created FileName, store in array of object
//         */
//        JSONArray jsonArray = new JSONArray();
//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("name",filename);
//            jsonObject.put("path",filetype);
//            jsonArray.put(jsonObject);
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//                     
            System.out.println("JSON OBJECT deLETE--");
                        JSONObject obj = new JSONObject();
                       obj.put("name", filename);
                       obj.put("path", path);
                       OkHttpClient.Builder builder = new OkHttpClient.Builder();
                        builder.connectTimeout(180, TimeUnit.SECONDS);
                        OkHttpClient client = builder.build();
                        RequestBody body = RequestBody.create(JSON, obj.toJSONString());
                        Request request = new Request.Builder()
                            .header("language", "en")
                            .header("device-type", "desktop")
                            .header("api-key", constant.Constant.API_KEY)
                        .url(constant.Constant.BASE_URL+apiCall)
                        .delete(body)
                        .build();
                        Response response = client.newCall(request).execute();
                    
                    return response.body().string();
                 }catch(Exception e)
                {
                    e.printStackTrace();
                }
                 return "";
    }
    private String checkTheFileType(boolean  filetype)
    {
        String serverAPI;
        if(filetype)
                {
                   serverAPI = constant.Constant.KEY_DELETE_FOLDER;
                   return serverAPI;
                }
                else
                {
                    serverAPI = constant.Constant.KEY_DELETE_FILE;
                    return serverAPI;
                }
        //constant.Constant.KEY_MOVETO_TRASH;
        //return serverAPI;
    }
    
}
