/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package constant;

/**
 *
 * @author vivek
 */
/**
 * This class defines all the constant and variables used for BoonDrive  Database
 * @author vivek
 */
public class DBConstant {
    
    public static String DATABASE_NAME = "BoondriveDesktop.sqlite";
    public static String TABLE_NAME_USERLOGIN = "userLogin";
    public static String TABLE_NAME_PATH = "path";
    
    public static String CREATE_TABLE_USERLOGIN = "CREATE TABLE IF NOT EXISTS userLogin(apiKey TEXT, email TEXT, firstName TEXT, "
              + "fullName TEXT, gender TEXT, lastName TEXT, professional INTEGER, "+ "profilePic TEXT, registered INTEGER, userId TEXT, userNumber INTEGER,"
              + " userSource TEXT, userType TEXT,isUserLoggedIn INTEGER,saved_path TEXT)";
    
    static final String DROP_TABLE = "DROP TABLE IF EXISTS ";
    public static String CREATE_TABLE_PATH = "CREATE TABLE path (created_path TEXT, deleted_path TEXT)";
   
}
